#! /bin/bash

orig=$1
name=$2

# Create a thorn $name_Helper

cp -R prototype/${orig}_Helper ${name}_Helper
find ${name}_Helper -name '*~' | rm -f
find ${name}_Helper -type f | xargs perl -pi -e "s/${orig}/${name}/g"
