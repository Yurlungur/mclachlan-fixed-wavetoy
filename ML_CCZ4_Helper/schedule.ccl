if (CCTK_EQUALS (evolution_method, "ML_CCZ4")) {
  
  if (timelevels == 1) {
    STORAGE: ADMBase::metric[1]
    STORAGE: ADMBase::curv[1]
    STORAGE: ADMBase::lapse[1]
    STORAGE: ADMBase::shift[1]
    STORAGE: ADMBase::dtlapse[1]
    STORAGE: ADMBase::dtshift[1]
  } else if (timelevels == 2) {
    STORAGE: ADMBase::metric[2]
    STORAGE: ADMBase::curv[2]
    STORAGE: ADMBase::lapse[2]
    STORAGE: ADMBase::shift[2]
    STORAGE: ADMBase::dtlapse[2]
    STORAGE: ADMBase::dtshift[2]
  } else if (timelevels == 3) {
    STORAGE: ADMBase::metric[3]
    STORAGE: ADMBase::curv[3]
    STORAGE: ADMBase::lapse[3]
    STORAGE: ADMBase::shift[3]
    STORAGE: ADMBase::dtlapse[3]
    STORAGE: ADMBase::dtshift[3]
  } else if (timelevels == 4) {
    #STORAGE: ADMBase::metric[4]
    #STORAGE: ADMBase::curv[4]
    #STORAGE: ADMBase::lapse[4]
    #STORAGE: ADMBase::shift[4]
    #STORAGE: ADMBase::dtlapse[4]
    #STORAGE: ADMBase::dtshift[4]
    STORAGE: ADMBase::metric[3]
    STORAGE: ADMBase::curv[3]
    STORAGE: ADMBase::lapse[3]
    STORAGE: ADMBase::shift[3]
    STORAGE: ADMBase::dtlapse[3]
    STORAGE: ADMBase::dtshift[3]
  }
  
  SCHEDULE ML_CCZ4_RegisterSlicing AT startup
  {
    LANG: C
    OPTIONS: meta
  } "Register slicing"
  
  SCHEDULE ML_CCZ4_SetGroupTags AT startup BEFORE Driver_Startup
  {
    LANG: C
    OPTIONS: meta
  } "Set checkpointing and prolongation group tags"
  
  SCHEDULE ML_CCZ4_RegisterConstrained IN MoL_Register
  {
    LANG: C
    OPTIONS: meta
  } "Register ADMBase variables as constrained"
  
  
  
  # Why would any of these two be necessary?
  #SCHEDULE GROUP ML_CCZ4_evolCalcGroup AT postinitial AFTER MoL_PostStep
  #{
  #} "Calculate BSSN RHS"
  #SCHEDULE GROUP MoL_CalcRHS AT postinitial AFTER MoL_PostStep
  #{
  #} "Evaluate RHS"
  
  SCHEDULE GROUP ML_CCZ4_evolCalcGroup IN MoL_CalcRHS
  {
  } "Calculate BSSN RHS"
  
  SCHEDULE GROUP ML_CCZ4_evolCalcGroup AT analysis
  {
    TRIGGERS: ML_CCZ4::ML_log_confacrhs
    TRIGGERS: ML_CCZ4::ML_metricrhs
    TRIGGERS: ML_CCZ4::ML_Gammarhs
    TRIGGERS: ML_CCZ4::ML_trace_curvrhs
    TRIGGERS: ML_CCZ4::ML_curvrhs
    TRIGGERS: ML_CCZ4::ML_lapserhs
    TRIGGERS: ML_CCZ4::ML_dtlapserhs
    TRIGGERS: ML_CCZ4::ML_shiftrhs
    TRIGGERS: ML_CCZ4::ML_dtshiftrhs
  } "Calculate BSSN RHS"
  
  
  
  if (CCTK_EQUALS (my_initial_boundary_condition, "extrapolate-gammas"))
  {
    SCHEDULE ML_CCZ4_ExtrapolateGammas AT initial AFTER ML_CCZ4_convertFromADMBaseGamma
    {
      LANG: C
      # We don't need to synchronise here because extrapolation is now filling
      # ghost zones
      #SYNC: ML_Gamma
      #SYNC: ML_dtlapse
      #SYNC: ML_dtshift
    } "Extrapolate Gammas and time derivatives of lapse and shift"
  }
  
  if (CCTK_EQUALS (my_rhs_boundary_condition, "NewRad"))
  {
    SCHEDULE ML_CCZ4_NewRad IN ML_CCZ4_evolCalcGroup AFTER ML_CCZ4_RHS
    {
      LANG: C
      #SYNC: ML_curvrhs
      #SYNC: ML_dtlapserhs
      #SYNC: ML_dtshiftrhs
      #SYNC: ML_Gammarhs
      #SYNC: ML_lapserhs
      #SYNC: ML_log_confacrhs
      #SYNC: ML_metricrhs
      #SYNC: ML_shiftrhs
      #SYNC: ML_trace_curvrhs
    } "Apply NewRad boundary conditions to RHS"
  }
  
  
  
  SCHEDULE GROUP ML_CCZ4_convertToADMBaseGroup IN ML_CCZ4_convertToADMBaseGroupWrapper
  {
    #SYNC: ADMBase::metric
    #SYNC: ADMBase::curv
    #SYNC: ADMBase::lapse
    #SYNC: ADMBase::shift
    #SYNC: ADMBase::dtlapse
    #SYNC: ADMBase::dtshift
  } "Calculate ADM variables"
  
  if (CCTK_EQUALS (calculate_ADMBase_variables_at, "MoL_PostStep"))
  {
    SCHEDULE GROUP ML_CCZ4_convertToADMBaseGroupWrapper IN MoL_PostStep AFTER (ML_CCZ4_ApplyBCs ML_CCZ4_enforce) BEFORE (ADMBase_SetADMVars Whisky_PostStep)
    {
    } "Calculate ADM variables"
  }
  else if  (CCTK_EQUALS (calculate_ADMBase_variables_at, "CCTK_EVOL"))
  {
    SCHEDULE GROUP ML_CCZ4_convertToADMBaseGroupWrapper IN MoL_PseudoEvolution BEFORE (ADMBase_SetADMVars Whisky_PostStep)
    {
    } "Calculate ADM variables"
  }
  else if  (CCTK_EQUALS (calculate_ADMBase_variables_at, "CCTK_ANALYSIS"))
  {
    SCHEDULE GROUP ML_CCZ4_convertToADMBaseGroupWrapper AT post_recover_variables
    {
    } "Calculate ADM variables"
    
    SCHEDULE GROUP ML_CCZ4_convertToADMBaseGroupWrapper AT analysis BEFORE (ADMBase_SetADMVars Whisky_PostStep)
    {
    } "Calculate ADM variables"
  }

  SCHEDULE ML_CCZ4_SelectBCsADMBase IN ML_CCZ4_convertToADMBaseGroupWrapper AFTER ML_CCZ4_convertToADMBaseGroup
  {
    LANG: C
    OPTIONS: level
  } "Select boundary conditions for ADMBase variables"
  
  SCHEDULE GROUP ApplyBCs AS ML_CCZ4_ApplyBCsADMBase IN ML_CCZ4_convertToADMBaseGroupWrapper AFTER ML_CCZ4_SelectBCsADMBase
  {
  } "Apply boundary conditions to ADMBase variables"
  
}
