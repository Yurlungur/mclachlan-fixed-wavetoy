#! /bin/bash

name=$1

# Create a thorn $name_Helper

orig=CL_BSSN

cp -R prototype/${orig}_Helper ${name}_Helper
find ${name}_Helper -name '*~' | xargs rm -f
find ${name}_Helper -type f | xargs perl -pi -e "s/${orig}/${name}/g"
